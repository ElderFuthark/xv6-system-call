#include "types.h"
#include "user.h"

//Program for testing our new syscall, howmanysys.
int main(int argc, char *argv[])
{
  printf(1, "The number of system calls made is %d\n", howmanysys());
  exit();
}
